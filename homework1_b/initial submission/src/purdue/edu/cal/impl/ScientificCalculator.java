package purdue.edu.cal.impl;

import static java.lang.Math.sqrt;

import purdue.edu.cal.base.Calculator;

public class ScientificCalculator extends BasicCalculator implements Calculator {

  public double squareRoot(double a) {
    return sqrt(a);
  }
  
  
}
