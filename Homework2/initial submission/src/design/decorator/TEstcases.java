package design.decorator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class TestCases {


  
  @Test
  void testAvatarCharater() throws Exception {
    Avatar av = new Male();
    assertEquals("Male ", av.decorateWith());
    
  }
  
  @Test
  void testWithEarrings() throws Exception {
    Avatar av = new Earrings(new Male());
    assertEquals("Earings ", av.item());
  }
  
  @Test
  void testWithHighHeels() throws Exception {
    Avatar av = new HighHeels(new Male());
    assertEquals("High Heels ", av.item());
  }
  
  @Test
  void testWithSweater() throws Exception {
    Avatar av = new Sweater(new Male());
    assertEquals("Sweater ", av.item());
  }
  
  
  @Test
  void testWithSweaterAndHeels() throws Exception {
    Avatar av = new Sweater(new Male());
    Avatar av1 = new HighHeels(av);
    
    assertEquals("Sweater High Heels ", av1.item());
  }
  
  
  
  
}
