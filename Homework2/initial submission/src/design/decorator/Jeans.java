package design.decorator;

public class Jeans extends AvatarDecorator {
  public Jeans(Avatar avatarDecorator) {
    super(avatarDecorator);
  }

  @Override
public String item() {
    // TODO Auto-generated method stu
    return super.item() + decorateWith();
  }
  
  public String decorateWith() {
    return "Jeans ";
  }
}