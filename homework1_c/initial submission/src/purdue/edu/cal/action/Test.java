package purdue.edu.cal.action;

import java.util.Scanner;
import purdue.edu.cal.base.Calculator;
import purdue.edu.cal.impl.AgebricCalculator;
import purdue.edu.cal.impl.ScientificCalculator;

public class Test {

/**
 * Main method also the action method.
 * @param args no argunments to pass
 */
public static void main(String[] args) {

    int a = 0;
    
    int b = 0;
    char ch;
    @SuppressWarnings("resource")
    Scanner sc = new Scanner(System.in);
    while (true) {
      
      System.out.println("Enter firt number");
      a = sc.nextInt();
      System.out.println("Enter second number");
      b = sc.nextInt();
      System.out.println("Enter operations like +,-,*,/,.,^");
      ch = sc.next().charAt(0);
      Calculator cal;
      switch (ch) {
        case '+':
          cal = new ScientificCalculator();
          System.out.println(cal.addition(a,b));
          break;

        case '-':
          cal = new ScientificCalculator();
          System.out.println(cal.subtraction(a,b));;
          break;

        case '*':
          cal = new ScientificCalculator();
          System.out.println(cal.multiplication(a,b));;
          break;

        case '/':
          cal = new ScientificCalculator();
          System.out.println(cal.division(a,b));;
          break;

        case '.':
          cal = new ScientificCalculator();
          System.out.println(((ScientificCalculator) cal).squareRoot(a));;
          break;
        
        case '^':
          cal = new AgebricCalculator();
          System.out.println(((AgebricCalculator) cal).power(a,b));;
          break;

        // operator doesn't match any case constant (+, -, *, /)
        default:
          System.out.printf("Error! operator is not correct");
          return;
      }

    }
    
    
    
    
  

  }

}
