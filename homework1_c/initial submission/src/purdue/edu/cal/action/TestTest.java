package purdue.edu.cal.action;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import purdue.edu.cal.impl.ScientificCalculator;

class TestTest {

  @Test
void test() {
	ScientificCalculator s = new ScientificCalculator();
  
    double output = s.addition(1.2,2.3);
    assertEquals(3.5,output);
  }
 
}
